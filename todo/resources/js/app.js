import Vue from "vue";
import App from "./components/App.vue";
import VueRouter from "vue-router";
import { routes } from "./routes/routes.js";
import { store } from "./store/store";

Vue.use(VueRouter);

const router = new VueRouter({
    routes,
    mode: "history"
});

const app = new Vue({
    el: "#app",
    router,
    store,
    render: h => h(App)
});
