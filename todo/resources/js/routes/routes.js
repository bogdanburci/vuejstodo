import ToDo from "../components/todo/ToDo.vue";
import AddNew from "../components/todo/AddNew.vue";
import Edit from "../components/todo/Edit.vue";
import Login from "../components/todo/Login.vue";
import Register from "../components/todo/Register.vue";
import Home from "../components/todo/Home.vue";

import { store } from "../store/store";

export const routes = [
    {
        path: "",
        name: "home",
        component: Home
    },
    {
        path: "/login",
        name: "login",
        component: Login
    },
    {
        path: "/todo",
        name: "todo",
        component: ToDo,
        beforeEnter(to, from, next) {
            if (store.state.loggedIn) {
                next();
            } else {
                next("/login");
            }
        }
    },
    {
        path: "/register",
        name: "register",
        component: Register
    },
    {
        path: "/add-new",
        component: AddNew,
        beforeEnter(to, from, next) {
            if (store.state.loggedIn) {
                next();
            } else {
                next("/login");
            }
        }
    },
    {
        path: "/edit/:id",
        component: Edit,
        beforeEnter(to, from, next) {
            if (store.state.loggedIn) {
                next();
            } else {
                next("/login");
            }
        }
    }
];
