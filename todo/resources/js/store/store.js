import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
axios.defaults.withCredentials = true;

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        loggedIn: false,
        taskList: [],
        confMessages: []
    },
    getters: {
        getTaskList: state => {
            return state.taskList;
        },
        getMessages: state => {
            return state.confMessages;
        },
        getLoggedStatus: state => {
            return state.loggedIn;
        }
    },
    mutations: {
        GET_ALL_TASKS: (state, payload) => {
            state.taskList = [...payload];
        },
        ADD_NEW_TASK: (state, payload) => {
            console.log(payload);
            state.taskList.push(payload);
        },
        TOGGLE_STATUS: (state, payload) => {
            let newTaskList = state.taskList.map(item => {
                if (item.id === payload.id) {
                    item.status = !item.status;
                }
                return item;
            });

            state.taskList = newTaskList;
            console.log(state);
        },
        DELETE_TASK: (state, payload) => {
            let newTaskList = state.taskList.filter(
                item => item.id != payload.id
            );

            state.taskList = newTaskList;
        },
        UPDATE_TASK: (state, payload) => {
            let newTaskList = state.taskList.map(item => {
                if (item.id === payload.id) {
                    item.name = payload.name;
                }
            });

            state = { ...state, taskList: newTaskList };
        },
        ADD_NEW_MESSAGE: (state, payload) => {
            let lastId =
                state.confMessages.length === 0
                    ? 0
                    : state.confMessages[state.confMessages.length - 1].id - 1;
            let message = Object.assign({ ...payload }, { id: lastId });

            state.confMessages.push(message);

            setTimeout(() => {
                state.confMessages.shift();
            }, 3000);
        },
        CHANGE_LOGGED_STATUS: (state, payload) => {
            state.loggedIn = payload;
        }
    },
    actions: {
        getAllTasks: ({ commit }, payload) => {
            axios.post("/api/tasks").then(response => {
                commit("GET_ALL_TASKS", response.data);
            });
        },
        addNewTask: ({ commit }, payload) => {
            axios
                .post("/api/add-task", payload)
                .then(response => {
                    commit("ADD_NEW_TASK", response.data);
                    commit("ADD_NEW_MESSAGE", {
                        message: `Task ${payload.task} created`,
                        class: "bg__add--new"
                    });
                })
                .catch(error => {
                    commit("ADD_NEW_MESSAGE", {
                        message: `Task field cannot be empty`,
                        class: "bg__deleted"
                    });
                });
        },
        toggleStatus: ({ commit }, payload) => {
            commit("TOGGLE_STATUS", payload);
            commit("ADD_NEW_MESSAGE", {
                message: `Task ${payload.id} updated to ${
                    payload.status ? "Done" : "Undone"
                }`,
                class: "bg__marked--done"
            });
        },
        deleteTask: ({ commit }, payload) => {
            if (confirm("Do you really want to delete?")) {
                axios
                    .post("/api/delete", payload)
                    .then(response => {
                        commit("DELETE_TASK", payload);
                        commit("ADD_NEW_MESSAGE", {
                            message: `Task ${payload.name} deleted`,
                            class: "bg__deleted"
                        });
                    })
                    .catch(error => {
                        commit("ADD_NEW_MESSAGE", {
                            message: `Error, something went wrong`,
                            class: "bg__deleted"
                        });
                        console.log(error);
                    });
            }
        },
        updateTask: ({ commit }, payload) => {
            return axios
                .post("/api/update", payload)
                .then(response => {
                    commit("UPDATE_TASK", response.data);
                    commit("ADD_NEW_MESSAGE", {
                        message: `Task ${payload.name} updated`,
                        class: "bg__updated"
                    });
                })
                .catch(error => {
                    console.log(error);
                });
        },

        loggIn: async ({ commit }, payload) => {
            try {
                const csrfCookie = await axios.get("/sanctum/csrf-cookie");
                const logResult = await axios.post("/login", payload);
                const userName = await axios.get("/api/user");

                if (userName.status === 200) {
                    commit("ADD_NEW_MESSAGE", {
                        message: `Welcome ${userName.data.name}`,
                        class: "bg__add--new"
                    });

                    commit("CHANGE_LOGGED_STATUS", true);
                } else {
                    commit("ADD_NEW_MESSAGE", {
                        message: `Error, something went wrong`,
                        class: "bg__deleted"
                    });
                    commit("CHANGE_LOGGED_STATUS", false);
                }

                return logResult;
            } catch (err) {
                let allErrors = err.response.data.errors;
                for (const msg in allErrors) {
                    let [errmsg] = allErrors[msg];
                    commit("ADD_NEW_MESSAGE", {
                        message: errmsg,
                        class: "bg__deleted"
                    });
                }
            }
        },

        logout: async () => {
            try {
                const logout = await axios.post("/logout");
                commit("CHANGE_LOGGED_STATUS", false);
            } catch (err) {
                console.log(err);
            }
        },
        register: async ({ commit }, payload) => {
            try {
                await axios.get("/sanctum/csrf-cookie");
                const register = await axios.post("/register", payload);
                commit("CHANGE_LOGGED_STATUS", false);
                return register;
            } catch (err) {
                let allErrors = err.response.data.errors;
                for (const msg in allErrors) {
                    let [errmsg] = allErrors[msg];
                    commit("ADD_NEW_MESSAGE", {
                        message: errmsg,
                        class: "bg__deleted"
                    });
                }
            }
        }
    }
});
