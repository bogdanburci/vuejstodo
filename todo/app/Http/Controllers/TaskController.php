<?php

namespace App\Http\Controllers;
use App\Task;
use Auth;

use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request){


        $userId=Auth::id();
        
        $tasks = Task::select('id','name','status')->where('user_id',$userId)->get();
       
        return response()->json($tasks);
    }

    public function store(Request $request){
        
        $validData = $request->validate([
            'task'=>'required',
         ]);

         $userId=Auth::id();

        $task= Task::firstOrCreate([
            'name'=>$request->task,
            'status' => 1,
            'user_id'=>$userId
        ])->only(['id','name','status']);

        return response()->json($task);
        
    }

    public function update(Request $request){
        
        $validData = $request->validate([
            'id'=>'required',
            'name'=>'required'
        ]);

        $task = Task::findOrFail($request->id);
        $task->name = $request->name;
        $task->save();

        return response()->json($task->only(['id','name','status']));
    }

    public function delete(Request $request){

        $validData = $request->validate([
            'id'=>'required'
         ]);

        $task = Task::find($request->id)->delete();

        return response()->json(['status'=>$task]);
    }
}
